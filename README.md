# GitLab Continuous Deployment (CD) Demo with Play, Scala, SBT & Heroku

[![build status](https://gitlab.com/jasperdenkers/lunatech-demo-cd-play-scala-heroku/badges/master/build.svg)](https://gitlab.com/jasperdenkers/lunatech-demo-cd-play-scala-heroku/commits/master)

This is an example project for a post on the Lunatech blog that demonstrates continuous deployment with Play, Scala, SBT & Heroku using GitLab's CI functionalities. A running version of this application can be found here: https://lunatech-demo-cd-play-scala.herokuapp.com/.

The post can be found here: http://lunatech.com/blog/V6g2MSsAAJpMjbFo/continuous-delivery-on-gitlab-with-play-scala-sbt-and-heroku.

## Getting Started

If you have [SBT](http://www.scala-sbt.org/) installed and this project checked out locally on your machine, you can execute the following commands in the root directory:

 - `sbt run`: Run the project on http://localhost:9000.
 - `sbt test`: Execute the project's tests.

## Deployment to Heroku

To be able to deploy this application you should create an application on Heroku and find your Heroku API key (under **[Account](https://dashboard.heroku.com/account)** > **API Key**).

The application name should be added in the line specifying deployment in `.gitlab-ci.yml`:

```
dpl --provider=heroku --app=<app-name> --api-key=$HEROKU_API_KEY
```

The API key should be added as a variable in your GitLab project settings (under Variables) with key `HEROKU_API_KEY`.

After a successful build and deploy your application will be available at `<app-name>.herokuapp.com`.
