name := """lunatech-demo-cd-play-scala-heroku"""

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % "test"

lazy val root = (project in file(".")).enablePlugins(PlayScala)
